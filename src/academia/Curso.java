package academia;



public class Curso {
	private String codigo;
	private String nombre;
	private int curso;
	private Asignatura[] asignaturas;
	private int numeroAsignaturas;


	public Curso(int curso, String codigo, String nombre) {
		super();
		this.curso = curso;
		this.codigo = codigo;
		this.nombre = nombre;
		asignaturas = new Asignatura[10];
		numeroAsignaturas = 0;
	}
	
	public void addAsignatura(Asignatura asignatura) {
		this.asignaturas[numeroAsignaturas] = asignatura;
		numeroAsignaturas++;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCurso() {
		return curso;
	}

	public void setCurso(int curso) {
		this.curso = curso;
	}
		
}

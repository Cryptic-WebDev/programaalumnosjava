package academia;

public class Nota {
	private double nota;
	private Asignatura asignatura;
	private Alumno alumno;
	

	public Nota(double nota, Asignatura asignatura, Alumno alumno) {
		this.nota = nota;
		this.asignatura = asignatura;
		this.alumno = alumno;
	}


	public double getNota() {
		return nota;
	}


	public void setNota(double nota) {
		this.nota = nota;
	}


	public Asignatura getAsignatura() {
		return asignatura;
	}


	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}


	public Alumno getAlumno() {
		return alumno;
	}


	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	
	
	
	
	
	
	
}

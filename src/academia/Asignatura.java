package academia;

public class Asignatura {
	private int codigo;
	private String nombre;
	private Curso curso;

	public Asignatura(int codigo, String nombre, Curso curso) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.curso = curso;
		
		
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	

}
